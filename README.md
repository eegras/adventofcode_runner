# Using this tool.

1. Check out the repo
2. Build a virtual environment, or simply install packages, that are in requirements.txt
3. Add your Advent of Code session token to the `AOC_SESSION_TOKEN` environment variable.  
3. Run `python aoc.py gen 2021` to generate the file structure for 2021.
4. In the `solution` folder, open `year_2021`, then open `day_2`, finally edit `part_1.py` to write your solution.  Example data that is given to you goes in `TEST_CASE`.  Their solution goes in `TEST_SOLUTION`.  Your code is inside `do_puzzle`.  Return your answer.  `puzzleinput` is a list of strings for each line of the input file.  Note it's strings.  If you're doing math, cast everything to ints or floats or whatever.  `puzzleinput = [int(i) for i in puzzleinput]` or whatever.
5. Run `python aoc.py run 2021 2 1`.  This runs the solution for year_2021, day_2, part_1.
