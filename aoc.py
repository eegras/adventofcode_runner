from typing import Dict, Any

import requests
import os
import sys
import time
import logging

solution_folder = 'solutions'

input_cache: dict[str, list] = {}

TEMPLATE = """# Advent Of Code {FILE_YEAR} {FILE_DAY} part {FILE_PART}

TEST_CASE = \"\"\"
\"\"\"

TEST_SOLUTION = ""

def do_puzzle(puzzleinput):
    return solution

"""


def get_from_aoc(year, day):
    # First check memory if the content lives there.
    if f"{year}-{day}" in input_cache:
        logging.info("Loaded input data from memory.")
        return input_cache[f"{year}-{day}"]

    # Next check filesystem.
    cache_file = os.path.join(solution_folder, f"year_{year}", f"day_{day}", f"input_data")
    if os.path.exists(cache_file):
        with open(cache_file) as file:
            logging.info("Loading input data from disk.")
            return file.read().splitlines()

    logging.info("Retrieving input data from AoC...")
    headers = {'Cookie': f"session={os.environ['AOC_SESSION_TOKEN']}"}
    my_input = requests.get(url=f"https://adventofcode.com/{year}/day/{day}/input", headers=headers)
    if my_input.status_code == 200:
        logging.info("Data retrieved.")
        input_cache[f"{year}-{day}"] = my_input.text.strip().split("\n")
        with open(cache_file, 'w+') as file:
            logging.info("Data saved to disk.")
            file.write("\n".join(input_cache[f"{year}-{day}"]))
        return input_cache[f"{year}-{day}"]
    raise ValueError(f"Retrieving data from AoC failed.  Check your token.  {my_input.status_code} - {my_input.text}")


def do_solution(solution_filename):
    solution_start = time.time()
    import_parts = solution_filename.split(".")
    solution_year, solution_day, solution_part = [f.split("_")[1] for f in import_parts if "_" in f]
    print(f"Problem {solution_year}/{solution_day.zfill(2)} part {solution_part}:")
    solution_file = __import__(solution_filename, fromlist=import_parts[0:-1])

    if hasattr(solution_file, 'TEST_CASE'):
        solution_value = solution_file.do_puzzle(solution_file.TEST_CASE.split("\n"))
        print(f"    Code Solution: {solution_value}")
        print(f"    Needed Solution: {solution_file.TEST_SOLUTION}")
        assert(solution_value == solution_file.TEST_SOLUTION)

    puzzle_input = get_from_aoc(solution_year, solution_day)
    solution_value = solution_file.do_puzzle(puzzle_input)

    if type(solution_value) == str:
        solution_string = solution_value.split("\n")
        solution_value = [solution_string[0], ]
        for s in solution_string[1:]:
            solution_value.append((' '*12) + str(s))
        solution_value = "\n".join(solution_value)

    print(f"    Execution duration: {time.time() - solution_start}s")
    print(f"    Result: {solution_value}")
    print()


def generate_year(FILE_YEAR):
    os.makedirs(os.path.join(solution_folder, f"year_{FILE_YEAR}"), exist_ok=True)
    for FILE_DAY in range(1, 26):
        os.makedirs(os.path.join(solution_folder, f"year_{FILE_YEAR}", f"day_{FILE_DAY}"), exist_ok=True)
        for FILE_PART in range(1, 3):
            project_file = os.path.join(solution_folder, f"year_{FILE_YEAR}", f"day_{FILE_DAY}", f"part_{FILE_PART}.py")
            if os.path.exists(project_file):
                continue

            with open(project_file, 'w+') as file:
                file.write(TEMPLATE.format(**locals()))


if __name__ == "__main__":
    if len(sys.argv) == 3 and sys.argv[1] == "gen":
        generate_year(sys.argv[2])
    elif len(sys.argv) == 5 and sys.argv[1] == "run":
        year = sys.argv[2]
        day = sys.argv[3]
        part = sys.argv[4]
        if part == "3":
            total_time_start = time.time()
            for i in range(1, 3):
                do_solution(f"{solution_folder}.year_{year}.day_{day}.part_{i}")
            print(f"Total execution duration: {time.time() - total_time_start}s")
        else:
            do_solution(f"{solution_folder}.year_{year}.day_{day}.part_{part}")
    else:
        print("Missing arguments.")
        print("-  aoc.py run <year> <day> <part>")
        print("-  aoc.py gen <year>")
