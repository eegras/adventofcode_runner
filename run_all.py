import aoc
import os

# First enumerate all solution years.
years = os.listdir(os.path.join('.', 'solutions'))
for year in years:
    # Get that year's days.
    days = os.listdir(os.path.join('.', 'solutions', year))
    for day in days:
        # Run each part.
        parts = os.listdir(os.path.join('.', 'solutions', year, day))
        for part in parts:
            if part.startswith("part_"):
                part, extension = part.split(".")
                try:
                    aoc.do_solution(f"solutions.{year}.{day}.{part}")
                except Exception as e:
                    print(f"solutions.{year}.{day}.{part} failed {e}")
                    pass
