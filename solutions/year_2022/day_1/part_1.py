from pprint import pprint
# Advent Of Code 2022 1 part 1

TEST_CASE = """1000
2000
3000

4000

5000
6000

7000
8000
9000

10000
"""

TEST_SOLUTION = 24000

def do_puzzle(puzzleinput):
    elves = []
    current_elf = 0
    for line in puzzleinput:
        if not line:
            elves.append(current_elf)
            current_elf = 0
        else:
            current_elf += int(line)

    solution = max(elves)
    return solution

