# Advent Of Code 2022 10 part 1

from collections import deque
from pprint import pprint, pformat
from copy import copy

TEST_CASE = """addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop
"""

TEST_SOLUTION = """##  ##  ##  ##  ##  ##  ##  ##  ##  ##  
###   ###   ###   ###   ###   ###   ### 
####    ####    ####    ####    ####    
#####     #####     #####     #####     
######      ######      ######      ####
#######       #######       #######     
 """

DISPLAY_CHAR = "###"
DISPLAY_DIMENSIONS = 39

def get_sprite_pos(sprite_pos):
    sprite_display = ["."]*40
    if (sprite_pos-1 >= 0):
        sprite_display[sprite_pos-1] = "#"
    sprite_display[sprite_pos] = "#"
    if (sprite_pos+1 < len(sprite_display)):
        sprite_display[sprite_pos+1] = "#"
    return "".join(sprite_display)

def do_puzzle(puzzleinput):
    solution = ""
    registers = {}
    registers['x'] = 1
    sequence = []
    debug_add = [1]
    cycle = 1
    desired_line = 0
    old_line = -1
    while (sequence or desired_line < len(puzzleinput)):
    #for line in puzzleinput:
        #print(f"Begin cycle {cycle}")
        print(f"Sprite position: {get_sprite_pos(registers['x'])}")
        print("")
        line = ""
        if desired_line < len(puzzleinput) and old_line != desired_line:
            old_line = desired_line
            line = puzzleinput[desired_line]

        #print(f"Start cycle  {str(cycle).rjust(5, ' ')}:")
        if line:
            exec_lifetime = 0
            exection_parts = line.split(" ")
            if exection_parts[0] == "noop":
                exec_lifetime = 1
            elif exection_parts[0] == "addx":
                exec_lifetime = 2

            if exec_lifetime:
                print(f"Start cycle   {str(cycle).rjust(5, ' ')}: begin executing {line}")
                sequence.append([line, exec_lifetime])
        else:
            print(f"Start cycle  {str(cycle).rjust(5, ' ')}:")

        """if (cycle - 20) % 40 == 0:
            print(f"During the {cycle}th cycle, register X has the value {registers['x']}, so the signal strength is {cycle} * {registers['x']} = {registers['x'] * cycle}. {pformat(sequence)}")
            print(debug_add)
        else:
            print(f"During the {cycle}th cycle, register X has the value {registers['x']}")"""

        new_sequence = []

        print(f"During cycle {str(cycle).rjust(5, ' ')}: CRT draws pixel in position {(cycle-1) % 40}")
        if registers['x'] - 1 <= (cycle-1) % 40 <= registers['x'] + 1:
            solution += "#"
        else:
            solution += " "

        print(f"Current CRT row: {solution}")


        for index, data_raw in enumerate(copy(sequence)):
            data = copy(data_raw)
            data[1] -= 1
            if data[1] == 0:
                # EXECUTE THIS
                executed_parts = data[0].split(" ")
                #pprint(executed_parts)
                if executed_parts[0] == "addx":
                    registers['x'] += int(executed_parts[1])
                    debug_add.append(int(executed_parts[1]))
                print(f"End of cycle {str(cycle).rjust(5, ' ')}: finish executing {data[0]} (Register X is now {registers['x']})")
            else:
                new_sequence.append(data)

        if not new_sequence:
            desired_line += 1
        #pprint(sequence)
        #pprint(new_sequence)
        #print()
        sequence = copy(new_sequence)

        #print(f"End cycle {cycle}")
        #print()

        cycle += 1

    solution = '\n'.join(solution[i:i + 40] for i in range(0, len(solution), 40))

    pprint(registers)
    return solution

