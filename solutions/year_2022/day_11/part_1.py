# Advent Of Code 2022 11 part 1
import math
from pprint import pprint

TEST_CASE = """Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1
"""

TEST_SOLUTION = 10605

def do_print(string):
    print(string)

monkies = {}

class Monkey(object):
    def __init__(self, starting_items=(), operation='', test='', test_true=0, test_false=0):
        self.items = list(starting_items)
        self._operation = ''
        self._test = '' # This will be eval'd.
        self.test_true = test_true
        self.test_false = test_false

    @property
    def test(self):
        return self._test
    @test.setter
    def test(self, test):
        test = test.strip()
        if not test:
            return

        test_parts = test.strip().split(" ")
        if test_parts[0] == "divisible":
            self._test = f"new % {test_parts[2]}"
        else:
            raise ValueError(f"Unknown test type! {test}")

    @property
    def operation(self):
        return self._operation
    @operation.setter
    def operation(self, operation):
        operation = operation.strip()

        if not operation:
            return

        if operation.startswith("new = "):
            self._operation = operation[6:]
        else:
            self._operation = operation
    def addItem(self, item):
        self.items.append(item)

    def testItems(self):
        destinations = []
        for old in self.items:
            do_print(f"  Monkey inspects an item with a worry level of {old}.")
            new = eval(self.operation)
            do_print(f"    Worry level is '{self.operation}' to {new}")
            new = math.floor(new / 3)
            do_print(f"    Monkey gets bored with item.  Worry level is divided by 3 to {new}")
            if eval(self.test) == 0:
                do_print(f"    Current worry level is '{self.test}'")
                do_print(f"    Item with worry level {new} thrown to monkey {self.test_true}")
                destinations.append((new, self.test_true))
            else:
                do_print(f"    Current worry level is not '{self.test}'")
                do_print(f"    Item with worry level {new} thrown to monkey {self.test_false}")
                destinations.append((new, self.test_false))
        self.items = []
        return destinations


def do_puzzle(puzzleinput):
    monkeys = {}
    current_monkey = 0

    for monkeytext in puzzleinput:
        if monkeytext.startswith("Monkey "):
            # Switching monkeys!
            _, monkey_id = monkeytext.split(" ")
            current_monkey = int(monkey_id.split(":")[0])
            monkeys[current_monkey] = Monkey()
        elif monkeytext.startswith("  Starting items: "):
            monkeytext = monkeytext.replace(",", "")
            monkeys[current_monkey].items = [int(f) for f in monkeytext.split(" ")[4:]]
        elif monkeytext.startswith("  Operation: "):
            monkeys[current_monkey].operation = monkeytext.split(":")[1]
        elif monkeytext.startswith("  Test: "):
            monkeys[current_monkey].test = monkeytext.split(":")[1]
        elif monkeytext.startswith("    If true: "):
            monkeys[current_monkey].test_true = int(monkeytext.split(" ")[-1])
        elif monkeytext.startswith("    If false: "):
            monkeys[current_monkey].test_false = int(monkeytext.split(" ")[-1])

    # All monkey objects have been created.
    item_interactions = {}
    for i in range(0, 20):
        for monkey in monkeys:
            do_print(f"Monkey {monkey}:")
            item_destinatons = monkeys[monkey].testItems()

            if monkey not in item_interactions:
                item_interactions[monkey] = 0

            item_interactions[monkey] += len(item_destinatons)
            for item, target_monkey in item_destinatons:
                monkeys[target_monkey].addItem(item)


    for monkey in monkeys:
        do_print(f"Monkey {monkey}: {monkeys[monkey].items}")

    monkey_business = list(item_interactions.values())
    monkey_business = sorted(monkey_business, reverse=True)

    solution = monkey_business[0] * monkey_business[1]
    return solution

