# Advent Of Code 2022 13 part 1
from pprint import pprint
from ast import literal_eval
from functools import cmp_to_key

TEST_CASE = """[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]
"""

TEST_SOLUTION = 140
#TEST_SOLUTION = 7

class SortedCorrectly(Exception):
    pass

class SortedIncorrectly(Exception):
    pass

def compare_ints(left_int, right_int, x=0):
    print(f'{" "*x} Compare {left_int} vs {right_int}')
    if left_int < right_int:
        print(f'{" " * x} Left side is smaller, so inputs are in the right order.')
        return 1
    elif left_int > right_int:
        print(f'{" " * x} Right side is smaller, so inputs are NOT in the right order.')
        return -1
    elif left_int == right_int:
        return 0

def compare_lists(left_list, right_list, x=0):
    """print(f'{" "*x}Compare {left_list} vs {right_list}')
    if left_list and not right_list or len(left_list) < len(right_list):
        raise SortedCorrectly(f'{" "*x} [{left_list},{right_list}]')
    elif not left_list and right_list or len(left_list) > len(right_list):
        raise SortedIncorrectly(f'{" "*x} [{left_list},{right_list}]')
    else:"""

    print(f'{" "*x} Comparing {left_list} and {right_list}')

    if left_list and not right_list:
        print(f'{" " * x} Right side ran out of items, so inputs are NOT in the right order.')
        return -1
    if not left_list and right_list:
        print(f'{" " * x} Left side ran out of items, so inputs are in the right order.')
        return 1

    for y in range(0, max(len(left_list), len(right_list))):
        left = None
        right = None
        if y < len(left_list):
            left = left_list[y]
        if y < len(right_list):
            right = right_list[y]

        if left == None and right:
            print(f'{" " * x} Left side ran out of items, so inputs are in the right order.')
            return 1
        if right == None and left:
            print(f'{" " * x} Right side ran out of items, so inputs are NOT in the right order.')
            return -1

        if type(left) == list and type(right) == list:
            return compare_lists(left, right, x+1)
        elif type(left) == int and type(right) == int:
            return compare_ints(left, right, x+1)

        if type(left) == list and type(right) == int:
            print(f'{" "*x}Mixed types; convert right to [{right}] and retry comparison')
            return compare_lists(left, [right, ], x+1)
        if type(left) == int and type(right) == list:
            print(f'{" "*x}Mixed types; convert left to [{left}] and retry comparison')
            return compare_lists([left, ], right, x+1)
    return 0

def do_puzzle(puzzleinput):
    solution = 0
    sorted_correct = 0
    sorted_incorrect = 0
    first_compare = "NO_VAL"
    second_compare = "NO_VAL"
    pair_count = 0
    DIVIDER_PACKET_1 = [[2]]
    DIVIDER_PACKET_2 = [[6]]
    signals = [DIVIDER_PACKET_1, DIVIDER_PACKET_2]
    puzzleinput.append("")
    for puzzlelist in puzzleinput:
        if puzzlelist:
            signals.append(literal_eval(puzzlelist))

    pprint(signals)
    signals = sorted(signals, key=cmp_to_key(compare_lists), reverse=True)

    solution = (signals.index(DIVIDER_PACKET_1) + 1) * (signals.index(DIVIDER_PACKET_2) + 1)

    return solution

