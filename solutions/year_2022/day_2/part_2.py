# Advent Of Code 2022 2 part 1

TEST_CASE = """A Y
B X
C Z"""

TEST_SOLUTION = 12

WINORDER = ['SCISSORS', 'PAPER', 'ROCK']

def do_puzzle(puzzleinput):
    # A X = ROCK
    # B Y = PAPER
    # C Z = SCISSORS

    moves = {'A': 'ROCK',
             'B': 'PAPER',
             'C': 'SCISSORS',
             'X': 1,
             'Y': 0,
             'Z': -1,
             }

    total_score_them = 0
    total_score_me = 0

    scores = {'ROCK': 1,
              'PAPER': 2,
              'SCISSORS': 3,
              'WIN': 6,
              'TIE': 3,
              'LOSS': 0}

    for line in puzzleinput:
        them, me = line.split(" ")
        them = moves[them]
        me_instruction = moves[me]

        them_position = WINORDER.index(them)
        me = WINORDER[(them_position + me_instruction) % 3]

        print(f"{them} {me_instruction} {me}")

        if them == me:
            # TIE
            total_score_them += scores[them] + scores['TIE']
            total_score_me += scores[me] + scores['TIE']
            print(f"TIE {scores[me] + scores['TIE']} {them} vs {me}")

        elif them == "ROCK":
            if me == "PAPER":
                total_score_them += scores[them] + scores['LOSS']
                total_score_me += scores[me] + scores['WIN']
                print(f"WIN {scores[me] + scores['WIN']} {them} vs {me}")
            elif me == "SCISSORS":
                total_score_them += scores[them] + scores['WIN']
                total_score_me += scores[me] + scores['LOSS']
                print(f"LOSS {scores[me] + scores['LOSS']} {them} vs {me}")

        elif them == "PAPER":
            if me == "ROCK":
                total_score_them += scores[them] + scores['WIN']
                total_score_me += scores[me] + scores['LOSS']
                print(f"LOSS {scores[me] + scores['LOSS']} {them} vs {me}")
            elif me == "SCISSORS":
                total_score_them += scores[them] + scores['LOSS']
                total_score_me += scores[me] + scores['WIN']
                print(f"WIN {scores[me] + scores['WIN']} {them} vs {me}")

        elif them == "SCISSORS":
            if me == "ROCK":
                total_score_them += scores[them] + scores['LOSS']
                total_score_me += scores[me] + scores['WIN']
                print(f"WIN {scores[me] + scores['WIN']} {them} vs {me}")
            elif me == "PAPER":
                total_score_them += scores[them] + scores['WIN']
                total_score_me += scores[me] + scores['LOSS']
                print(f"LOSS {scores[me] + scores['LOSS']} {them} vs {me}")

    solution = total_score_me
    return solution

