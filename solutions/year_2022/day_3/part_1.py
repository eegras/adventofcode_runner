# Advent Of Code 2022 3 part 1

TEST_CASE = """vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw
"""

TEST_SOLUTION = 157

priorities = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

def do_puzzle(puzzleinput):
    solution = 0
    for rucksack in puzzleinput:
        if not rucksack:
            continue

        half = int(len(rucksack) / 2)
        first_pocket = set(rucksack[0:half])
        second_pocket = set(rucksack[half:])
        try:
            shared = list(first_pocket.intersection(second_pocket))[0]
        except IndexError:
            print(first_pocket)
            print(second_pocket)
        solution += priorities.index(shared)

    return solution

