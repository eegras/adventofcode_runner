# Advent Of Code 2022 3 part 1

TEST_CASE = """vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw
"""

TEST_SOLUTION = 70

priorities = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

def do_puzzle(puzzleinput):
    solution = 0
    rucksacks = []
    for rucksack in puzzleinput:
        print(rucksack)
        if not rucksack:
            continue

        rucksacks.append(rucksack)
        if len(rucksacks) == 3:
            print(rucksacks)
            first_rucksack = set(rucksacks[0])
            second_rucksack = set(rucksacks[1])
            third_rucksack = set(rucksacks[2])
            shared = list(first_rucksack.intersection(second_rucksack, third_rucksack))[0]
            print(shared)
            solution += priorities.index(shared)
            rucksacks = []

    return solution

