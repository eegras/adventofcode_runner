# Advent Of Code 2022 4 part 1

TEST_CASE = """2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8
"""

TEST_SOLUTION = 4

def do_puzzle(puzzleinput):
    solution = 0
    for assignments in puzzleinput:
        if not assignments:
            continue

        first_elf, second_elf = assignments.split(",")
        first_elf_start, first_elf_end = first_elf.split("-")
        second_elf_start, second_elf_end = second_elf.split("-")
        first_elf_range = set(range(int(first_elf_start), int(first_elf_end) + 1))
        second_elf_range = set(range(int(second_elf_start), int(second_elf_end) + 1))

        if first_elf_range.intersection(second_elf_range) or second_elf_range.intersection(first_elf_range):
            solution += 1

    return solution

