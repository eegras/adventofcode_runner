from pprint import pprint
import re
# Advent Of Code 2022 5 part 1


TEST_CASE = """    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
"""

TEST_SOLUTION = "MCD"

def do_puzzle(puzzleinput):
    stack_state = {}
    finding_stacks = True
    for instruction in puzzleinput:
        if not instruction and finding_stacks:
            for i in stack_state:
                stack_state[i].pop()
                stack_state[i].reverse()
            finding_stacks = False
            continue

        if finding_stacks:
            # Finding initial stack state.
            instruction_length = 4
            rows = [ instruction[i:i+instruction_length] for i in range(0, len(instruction), instruction_length) ]
            for i, container in enumerate(rows):
                i = i+1
                if i not in stack_state:
                    stack_state[i] = []

                container = re.sub(r'\W+', '', container)
                if container:
                    stack_state[i].append(container)
        else:
            # Doing moves now.
            if not instruction:
                continue

            _, amount, _, source, _, destination = instruction.split(" ")
            amount = int(amount)
            source = int(source)
            destination = int(destination)

            print(f"_ moving {amount} from {source} to {destination}")
            start_idx = len(stack_state[source])-amount
            stack_state[destination].extend(stack_state[source][start_idx:])
            del stack_state[source][start_idx:]

    solution = ""
    for row in stack_state:
        solution += stack_state[row][-1]


    return solution

