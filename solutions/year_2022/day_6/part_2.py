from collections import deque
# Advent Of Code 2022 6 part 1

TEST_CASE = """mjqjpqmgbljsphdztnvjfqwrcgsmlb
"""

TEST_SOLUTION = 19


def do_puzzle(puzzleinput):
    def check_solution(puzzle_bytes):
        chars = []
        for byte_character in puzzle_bytes:
            if byte_character in chars:
                return False
            chars.append(byte_character)
        return True

    current_byte = deque(maxlen=14)
    i = 0
    for character in puzzleinput[0]:
        i += 1
        current_byte.append(character)

        if len(current_byte) < current_byte.maxlen:
            continue

        if check_solution(current_byte):
            return i
